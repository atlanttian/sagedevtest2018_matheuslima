# Definição do Projeto Desktop #

### Prazo de Entrega ###
* 24/07/2018

### Poderão ser utilizadas as seguintes IDEs  ###
* Delphi 7
* Delphi 10.2
* Visual Studio

### Poderão ser utilizados os seguintes bancos de dados ###
* SQL Server
* MySQL

### Especificação do Projeto ###
* O projeto consiste na importação do arquivo ITEM.txt seguindo o layout disponibilizado no link abaixo:
* [http://ajudaonline.ebs.com.br/sgc/index.html?cefoutimpprolayo.htm](http://ajudaonline.ebs.com.br/sgc/index.html?cefoutimpprolayo.htm);
* O arquivo em anexo é um exemplo, mas o sistema deve permitir a importação de qualquer arquivo que siga este layout.

### Critérios para Aceitação ###
* Deverá ser criado um banco de dados para armazenar as informações;
* Anexar neste projeto o script de criação do banco de dados;
* Deverá ser implementada uma tela para importação do arquivo, onde será solicitado o diretório do arquivo;
* Deverão, no mínimo, ser importados os registros UNIDADES DE MEDIDA e CADASTRO DE ITEM;
* Todos os campos deverão ser validados conforme a coluna "Comentários" do [layout](http://ajudaonline.ebs.com.br/sgc/index.html?cefoutimpprolayo.htm);
* Exibir erros encontrados no caso de falhas na validação dos campos;
* Implementar telas para consulta das informações que foram importadas do arquivo texto, com filtros;
* Disponibilizar o executável e dependências da aplicação desenvolvida.

### Utilização do BitBucket ###
* Criar um Fork a partir do projeto;
* Após a conclusão do projeto, submeter o código através de um Pull Request;
* Para facilitar pode ser utilizada a ferramenta SourceTree.