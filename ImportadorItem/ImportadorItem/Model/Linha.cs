﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportadorItem.Model
{
    public class Linha
    {
        public int Numero { get; set; }
        public string LinhaCompleta { get; set; }
        public int? Tipo { get; set; }
        public int? Sequencia { get; set; }
        public bool ExisteErro { get; private set; }
        public string Mensagem { get; private set; }

        public Linha() { }

        public Linha(int nLinha)
        {
            Numero = nLinha;
        }

        public Linha ReportarErro( string mensagem)
        {
            ExisteErro = true;
            if (string.IsNullOrWhiteSpace(Mensagem))
            {
                Mensagem = mensagem;
            }
            else
            {
                Mensagem += $"\n{mensagem}";
            }
            return this;
        }
    }
}
