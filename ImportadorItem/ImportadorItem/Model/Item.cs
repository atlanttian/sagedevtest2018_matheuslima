﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportadorItem.Model
{
    public class Item : Linha
    {
        public Item(Linha linha = null)
        {
            if (linha != null)
            {
                Numero = linha.Numero;
                LinhaCompleta = linha.LinhaCompleta;
                Tipo = linha.Tipo;
                Sequencia = linha.Sequencia;
            }
        }

        public uint Codigo { get; set; }
        public string Descricao { get; set; }
        public int NCM { get; set; }
        public string Unidade { get; set; }
        public float Peso { get; set; }
        public string Identificacao { get; set; }
        public int TipoDoProduto { get; set; }
        public int? EntradaICMSST { get; set; }
        public int? EntradaIPIST { get; set; }
        public int? SaidaICMSST { get; set; }
        public int? SaidaIPIST { get; set; }
        public float AliquotaICMS { get; set; }
        public float AliquotaIPI { get; set; }
        public bool? SimplesNacional { get; set; }
        public string GTIN { get; set; }
        public int? EntradaCSTPIS { get; set; }
        public int? EntradaCSTCOFINS { get; set; }
        public int? SaidaCSTPIS { get; set; }
        public int? SaidaCSTCOFINS { get; set; }
        public string CodigoPISCOFINS { get; set; }
        public float? AliquotaPIS { get; set; }
        public float? AliquotaCOFINS { get; set; }
        public uint? ANP { get; set; }
        public int? CEST { get; set; }
        public int? TipoMedicamento { get; set; }
        public int? ReferenciaBC { get; set; }
    }
}
