﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportadorItem.Model
{
    public class FatorDeConversao : Linha
    {
        public FatorDeConversao(Linha linha = null)
        {
            if (linha != null)
            {
                Numero = linha.Numero;
                LinhaCompleta = linha.LinhaCompleta;
                Tipo = linha.Tipo;
                Sequencia = linha.Sequencia;
            }
        }

        public uint Codigo { get; set; }
        public string Identificaco { get; set; }
        public string Unidade { get; set; }
        public float Fator { get; set; }
    }
}
