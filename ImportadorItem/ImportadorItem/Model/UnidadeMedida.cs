﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportadorItem.Model
{
    public class UnidadeMedida : Linha
    {
        public UnidadeMedida(Linha linha = null)
        {
            if (linha != null)
            {
                Numero = linha.Numero;
                LinhaCompleta = linha.LinhaCompleta;
                Tipo = linha.Tipo;
                Sequencia = linha.Sequencia;
            }
        }

        public string Unidade { get; set; }
        public string Descricao { get; set; }
    }
}
