﻿using ImportadorItem.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace ImportadorItem.DAL
{
    public static class DatabaseControl
    {
        private const string INSERT_UNIDADE = "INSERT INTO [dbo].[Unidade] ([Unidade],[Descricao]) VALUES (@unidade , @descricao);";
        private const string INSERT_ITEM = "INSERT INTO [dbo].[Item] " +
            "([Codigo],[Descricao],[NCM],[Unidade],[Peso],[Identificacao],[TipoDoProduto],[EntradaICMSST],[EntradaIPIST],[SaidaICMSST],[SaidaIPIST],[AliquotaICMS],[AliquotaIPI],[SimplesNacional],[GTIN],[EntradaCSTPIS],[EntradaCSTCOFINS],[SaidaCSTPIS],[SaidaCSTCOFINS],[CodigoPISCOFINS],[AliquotaPIS],[AliquotaCOFINS],[ANP],[CEST],[TipoMedicamento],[ReferenciaBC]) " +
            "VALUES " +
            "(@codigo, @descricao, @ncm, @unidade, @peso, @identificacao, @tipodoproduto, @entradaicmsst, @entradaipist, @saidaicmsst, @saidaipist, @aliquotaicms, @aliquotaipi, @simplesnacional, @gtin, @entradacstpis, @entradacstcofins, @saidacstpis, @saidacstcofins, @codigopiscofins, @aliquotapis, @aliquotacofins, @anp, @cest, @tipomedicamento, @referenciabc); ";
        private const string INSERT_FATOR = "INSERT INTO [dbo].[FatorDeConversao]([Codigo],[Identificacao],[Unidade],[Fator]) VALUES (@codigo, @identificacao, @unidade, @fator);";

        private const string VERIFICA_CODIGO_ITEM = "SELECT 0 FROM Item WHERE Codigo = @codigo;";
        private const string VERIFICA_UNIDADE = "SELECT 0 FROM Unidade WHERE Unidade = @unidade;";
        private const string VERIFICA_CODIGO_FATOR = "SELECT 0 FROM FatorDeConversao WHERE Codigo = @codigo;";
        

        public static bool ExisteUnidade(string unidade)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ImportadorItem.Properties.Settings.DBCS"].ConnectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(VERIFICA_UNIDADE, con))
                {
                    command.Parameters.Add(new SqlParameter("@unidade", unidade));

                    return command.ExecuteScalar() != null;
                }
            }
        }

        public static bool ExisteItem(uint codigo)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ImportadorItem.Properties.Settings.DBCS"].ConnectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(VERIFICA_CODIGO_ITEM, con))
                {
                    command.Parameters.Add(new SqlParameter("@codigo", Convert.ToInt64(codigo)));

                    return command.ExecuteScalar() != null;
                }
            }
        }

        public static bool ExisteFator(uint codigo)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ImportadorItem.Properties.Settings.DBCS"].ConnectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(VERIFICA_CODIGO_FATOR, con))
                {
                    command.Parameters.Add(new SqlParameter("@codigo", Convert.ToInt64(codigo)));

                    return command.ExecuteScalar() != null;
                }
            }
        }

        public static void InserirUnidade(UnidadeMedida unidade)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ImportadorItem.Properties.Settings.DBCS"].ConnectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(INSERT_UNIDADE, con))
                {
                    command.Parameters.Add(new SqlParameter("@unidade", unidade.Unidade));

                    if (string.IsNullOrWhiteSpace(unidade.Descricao))
                        command.Parameters.Add(new SqlParameter("@descricao", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@descricao", unidade.Descricao));

                    command.ExecuteNonQuery();
                }
            }
        }

        public static void InserirItem(Item item)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ImportadorItem.Properties.Settings.DBCS"].ConnectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(INSERT_ITEM, con))
                {
                    command.Parameters.Add(new SqlParameter("@codigo", Convert.ToInt64(item.Codigo)));
                    if(string.IsNullOrWhiteSpace(item.Descricao))
                        command.Parameters.Add(new SqlParameter("@descricao", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@descricao", item.Descricao));
                    command.Parameters.Add(new SqlParameter("@ncm", item.NCM));
                    if(string.IsNullOrWhiteSpace(item.Unidade))
                        command.Parameters.Add(new SqlParameter("@unidade", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@unidade", item.Unidade));
                    command.Parameters.Add(new SqlParameter("@peso", item.Peso));
                    if(string.IsNullOrWhiteSpace(item.Identificacao))
                        command.Parameters.Add(new SqlParameter("@identificacao", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@identificacao", item.Identificacao));
                    command.Parameters.Add(new SqlParameter("@tipodoproduto", item.TipoDoProduto));
                    if (item.EntradaICMSST == null)
                        command.Parameters.Add(new SqlParameter("@entradaicmsst", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@entradaicmsst", item.EntradaICMSST));
                    if (item.EntradaIPIST == null)
                        command.Parameters.Add(new SqlParameter("@entradaipist", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@entradaipist", item.EntradaIPIST));
                    if (item.SaidaICMSST == null)
                        command.Parameters.Add(new SqlParameter("@saidaicmsst", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@saidaicmsst", item.SaidaICMSST));
                    if (item.SaidaIPIST == null)
                        command.Parameters.Add(new SqlParameter("@saidaipist", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@saidaipist", item.SaidaIPIST));

                    command.Parameters.Add(new SqlParameter("@aliquotaicms", item.AliquotaICMS));
                    command.Parameters.Add(new SqlParameter("@aliquotaipi", item.AliquotaIPI));
                    if (item.SimplesNacional == null)
                        command.Parameters.Add(new SqlParameter("@simplesnacional", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@simplesnacional", (item.SimplesNacional == true ? 'S' : 'N')));
                    if(string.IsNullOrWhiteSpace(item.GTIN))
                        command.Parameters.Add(new SqlParameter("@gtin", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@gtin", item.GTIN));
                    if (item.EntradaCSTPIS == null)
                        command.Parameters.Add(new SqlParameter("@entradacstpis", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@entradacstpis", item.EntradaCSTPIS));
                    if (item.EntradaCSTCOFINS == null)
                        command.Parameters.Add(new SqlParameter("@entradacstcofins", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@entradacstcofins", item.EntradaCSTCOFINS));
                    if (item.SaidaCSTPIS == null)
                        command.Parameters.Add(new SqlParameter("@saidacstpis", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@saidacstpis", item.SaidaCSTPIS));
                    if (item.SaidaCSTCOFINS == null)
                        command.Parameters.Add(new SqlParameter("@saidacstcofins", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@saidacstcofins", item.SaidaCSTCOFINS));
                    if (string.IsNullOrWhiteSpace(item.CodigoPISCOFINS))
                        command.Parameters.Add(new SqlParameter("@codigopiscofins", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@codigopiscofins", item.CodigoPISCOFINS));
                    if (item.AliquotaPIS == null)
                        command.Parameters.Add(new SqlParameter("@aliquotapis", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@aliquotapis", item.AliquotaPIS));
                    if (item.AliquotaCOFINS == null)
                        command.Parameters.Add(new SqlParameter("@aliquotacofins", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@aliquotacofins", item.AliquotaCOFINS));
                    if (item.ANP == null)
                        command.Parameters.Add(new SqlParameter("@anp", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@anp", Convert.ToInt64(item.ANP)));
                    if (item.CEST == null)
                        command.Parameters.Add(new SqlParameter("@cest", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@cest", item.CEST));
                    if (item.TipoMedicamento == null)
                        command.Parameters.Add(new SqlParameter("@tipomedicamento", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@tipomedicamento", item.TipoMedicamento));
                    if (item.ReferenciaBC == null)
                        command.Parameters.Add(new SqlParameter("@referenciabc", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@referenciabc", item.ReferenciaBC));

                    command.ExecuteNonQuery();
                }
            }
        }

        public static void InserirFator(FatorDeConversao fator)
        {
            using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ImportadorItem.Properties.Settings.DBCS"].ConnectionString))
            {
                con.Open();
                using (SqlCommand command = new SqlCommand(INSERT_FATOR, con))
                {
                    command.Parameters.Add(new SqlParameter("@codigo", Convert.ToInt64(fator.Codigo)));
                    if (fator.Identificaco == null)
                        command.Parameters.Add(new SqlParameter("@identificacao", DBNull.Value));
                    else
                        command.Parameters.Add(new SqlParameter("@identificacao", fator.Identificaco));
                    command.Parameters.Add(new SqlParameter("@unidade", fator.Unidade));
                    command.Parameters.Add(new SqlParameter("@fator", fator.Fator));

                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
