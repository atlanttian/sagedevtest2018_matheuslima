﻿using ImportadorItem.Control;
using ImportadorItem.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImportadorItem.View
{
    /// <summary>
    /// Interaction logic for ImportarItens.xaml
    /// </summary>
    public partial class ImportarItens : Window, INotifyPropertyChanged
    {

        private string caminho;
        private int numeroLinhas;
        private BackgroundWorker worker;
        public ObservableCollection<Linha> Erros { get; set; }

        public ImportarItens()
        {
            worker = new BackgroundWorker { WorkerReportsProgress = true, WorkerSupportsCancellation = true };

            if (worker != null)
            {
                worker.ProgressChanged += ReportarProgresso;
                worker.DoWork += Validar;
                worker.RunWorkerCompleted += Finalizar;
            }

            NumeroLinhas = 1;
            InitializeComponent();
            Erros = new ObservableCollection<Linha>();
            DataContext = this;
        }

        public string Caminho
        {
            get { return caminho; }
            set
            {
                if (caminho != value)
                {
                    caminho = value;
                    NotifyPropertyChanged();
                }

                //validação da ui
                PBProgresso.Value = 0;
                BImportar.IsEnabled = false;
            }
        }

        public int NumeroLinhas
        {
            get { return numeroLinhas; }
            set
            {
                if (numeroLinhas != value)
                {
                    numeroLinhas = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                CheckFileExists = true,
                Filter = "Text files(*.txt)| *.txt",
                Multiselect = false
            };
            if (dialog.ShowDialog() == true)
            {
                Caminho = dialog.FileName;
            }
        }

        private bool HasError(DependencyObject obj)
        {
            return Validation.GetHasError(obj) &&
            LogicalTreeHelper.GetChildren(obj)
            .OfType<DependencyObject>()
            .All(HasError);
        }

        private void BValidar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!HasError(TBCaminho as DependencyObject))
                {
                    GBArquivo.IsEnabled = false;
                    BCancelarValidacao.IsEnabled = true;
                    PBProgresso.IsIndeterminate = true;
                    Erros.Clear();

                    worker.RunWorkerAsync(false);
                }
                else
                {
                    MessageBox.Show(this, "Não foi possivel inicializar a validação pois o caminho do arquivo não está valido.", "Alerta", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        

        private void Finalizar(object sender, RunWorkerCompletedEventArgs e)
        {
            GBArquivo.IsEnabled = true;
            BCancelarValidacao.IsEnabled = false;
            PBProgresso.IsIndeterminate = false;
            BImportar.IsEnabled = false;

            if (e.Error == null)
            {
                bool importacao = false;
                if (e.Result is bool b)
                    importacao = b;

                if (Erros.Count < 1 & !importacao)
                {
                    MessageBox.Show(this, "O arquivo foi validado com sucesso.\nImportação habilitada.", "Informação", MessageBoxButton.OK, MessageBoxImage.Information);
                    BImportar.IsEnabled = true;
                }
                else if (importacao)
                    MessageBox.Show(this, "Importação concluida", "Importação", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
                MessageBox.Show(this, e.Error.Message, "Erro", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void Validar(object sender, DoWorkEventArgs e)
        {
            bool importar = false;

            if (e.Argument is bool b)
                importar = b;

            e.Result = importar; //usado para validar a menssagem no evento de finalização

            if (sender is BackgroundWorker w)
            {
                int nLinhas = 0;
                try
                {
                    using (StreamReader arquivo = File.OpenText(Caminho))
                    {
                        string linhaCompleta;
                        while ((linhaCompleta = arquivo.ReadLine()) != null && !w.CancellationPending)
                        {
                            nLinhas++;

                            Linha linha = Linhas.Validar(linhaCompleta, nLinhas);
                            
                            if (importar & !linha.ExisteErro & string.IsNullOrWhiteSpace(linha.Mensagem))
                                Linhas.Importar(linha);

                            w.ReportProgress(nLinhas, linha);

                        }
                        NumeroLinhas = nLinhas;
                        arquivo.Close();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"{ex.Message}\nNa linha:{nLinhas}");
                }
            }
            else
            {
                throw new ArgumentException("Erro ao inicializar a validação.");
            }
        }

        private void ReportarProgresso(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is Linha l)
            {
                if (l.ExisteErro)
                {
                    Erros.Add(l);
                    DGInvalidos.ScrollIntoView(Erros[Erros.Count - 1]);
                }
            }
            if (e.ProgressPercentage > -1)
            {
                PBProgresso.Value = e.ProgressPercentage;
            }
        }

        private void BCancelarValidacao_Click(object sender, RoutedEventArgs e)
        {
            worker.CancelAsync();
        }

        private void BImportar_Click(object sender, RoutedEventArgs e)
        {
            GBArquivo.IsEnabled = false;

            worker.RunWorkerAsync(true);
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (worker.IsBusy)
            {
                MessageBox.Show(this, "Não é possivel fechar a janela enquanto estiver em processo de Validação/Importação.", "Finalizar", MessageBoxButton.OK, MessageBoxImage.Information);
                e.Cancel = true;
            }
        }
    }

    internal class ValidaArquivo : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            if (!string.IsNullOrWhiteSpace(value?.ToString()))
            {
                if (File.Exists(value?.ToString()))
                {
                    if (value.ToString().EndsWith(".txt", StringComparison.OrdinalIgnoreCase))
                    {
                        return ValidationResult.ValidResult;
                    }
                    else
                    {
                        return new ValidationResult(false, "Somente serão aceitos arquivos com a extenção 'txt'.\nExemplo: ITEM.txt");
                    }
                }
                else
                {
                    return new ValidationResult(false, $"O caminho {value.ToString()} não é um caminho válido.");
                }
            }
            return new ValidationResult(false, $"O caminho não pode estar em branco.");
        }
    }
}
