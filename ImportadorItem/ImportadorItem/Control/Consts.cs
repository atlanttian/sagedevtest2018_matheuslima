﻿namespace ImportadorItem.Control
{
    internal static class Consts
    {

        internal const int QUANTIDADE_DE_CARACTERES = 500;

        internal const string SEQUENCIA = "SEQUÊNCIA";
        internal const int SEQUENCIA_INICIO = 494;
        internal const int SEQUENCIA_TAMANHO = 6;

        internal const string TIPO_DE_REGISTRO = "TIPO DE REGISTRO";
        internal readonly static int[] TIPO_DE_REGISTROS = { 1, 2, 3};
        internal const int TIPO_DE_REGISTRO_INICIO = 493;
        internal const int TIPO_DE_REGISTRO_TAMANHO = 1;

        #region UNIDADE

        internal const string UNIDADE = "UNIDADE";
        internal const int UNIDADE_INICIO = 0;
        internal const int UNIDADE_TAMANHO = 6;

        internal const string UNIDADE_DESCRICAO = "DESCRIÇÃO";
        internal const int UNIDADE_DESCRICAO_INICIO = 6;
        internal const int UNIDADE_DESCRICAO_TAMANHO = 30;

        internal const string UNIDADE_BRANCOS = "BRANCOS";
        internal const int UNIDADE_BRANCOS_INICIO = 36;
        internal const int UNIDADE_BRANCOS_TAMANHO = 457;

        #endregion

        #region ITEM

        internal const string ITEM_CODIGO = "CÓDIGO";
        internal const int ITEM_CODIGO_INICIO = 0;
        internal const int ITEM_CODIGO_TAMANHO = 10;
        internal const uint ITEM_CODIGO_MAXIMO = 2147483648;

        internal const string ITEM_DESCRICAO = "DESCRIÇÃO";
        internal const int ITEM_DESCRICAO_INICIO = 10;
        internal const int ITEM_DESCRICAO_TAMANHO = 40;

        internal const string ITEM_NCM = "NCM";
        internal const int ITEM_NCM_INICIO = 50;
        internal const int ITEM_NCM_TAMANHO = 8;

        internal const string ITEM_UNIDADE1 = "UNIDADE1";
        internal const int ITEM_UNIDADE1_INICIO = 58;
        internal const int ITEM_UNIDADE1_TAMANHO = 4;

        internal const string ITEM_PESO = "PESO";
        internal const int ITEM_PESO_INICIO = 62;
        internal const int ITEM_PESO_TAMANHO = 9;
        internal const int ITEM_PESO_DECIMAIS = 3;

        internal const string ITEM_IDENTIFICACAO = "IDENTIFICAÇÃO";
        internal const int ITEM_IDENTIFICACAO_INICIO = 71;
        internal const int ITEM_IDENTIFICACAO_TAMANHO = 15;

        internal const string ITEM_TIPO_DO_PRODUTO = "TIPO DO PRODUTO";
        internal const int ITEM_TIPO_DO_PRODUTO_INICIO = 86;
        internal const int ITEM_TIPO_DO_PRODUTO_TAMANHO = 2;
        internal static int[] ITEM_TIPOS_DO_PRODUTO = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 99 };

        internal const string ITEM_BRANCOS1 = "BRANCOS";
        internal const int ITEM_BRANCOS1_INICIO = 88;
        internal const int ITEM_BRANCOS1_TAMANHO = 12;

        internal const string ITEM_ENTRADA_ICMS = "ENTRADA - SIT.TRIB.ICMS";
        internal const int ITEM_ENTRADA_ICMS_INICIO = 100;
        internal const int ITEM_ENTRADA_ICMS_TAMANHO = 3;

        internal const string ITEM_ENTRADA_IPI = "ENTRADA - SIT.TRIB.IPI";
        internal const int ITEM_ENTRADA_IPI_INICIO = 103;
        internal const int ITEM_ENTRADA_IPI_TAMANHO = 3;

        internal const string ITEM_SAIDA_ICMS = "SAÍDA - SIT.TRIB.ICMS";
        internal const int ITEM_SAIDA_ICMS_INICIO = 106;
        internal const int ITEM_SAIDA_ICMS_TAMANHO = 3;

        internal const string ITEM_SAIDA_IPI = "SAÍDA - SIT.TRIB.IPI";
        internal const int ITEM_SAIDA_IPI_INICIO = 109;
        internal const int ITEM_SAIDA_IPI_TAMANHO = 3;

        internal const string ITEM_UNIDADE2 = "UNIDADE2";
        internal const int ITEM_UNIDADE2_INICIO = 112;
        internal const int ITEM_UNIDADE2_TAMANHO = 6;

        internal const string ITEM_ALIQUOTA_ICMS = "ALÍQUOTA ICMS";
        internal const int ITEM_ALIQUOTA_ICMS_INICIO = 118;
        internal const int ITEM_ALIQUOTA_ICMS_TAMANHO = 5;
        internal const int ITEM_ALIQUOTA_ICMS_DECIMAIS = 2;

        internal const string ITEM_ALIQUOTA_IPI = "ALÍQUOTA IPI";
        internal const int ITEM_ALIQUOTA_IPI_INICIO = 123;
        internal const int ITEM_ALIQUOTA_IPI_TAMANHO = 5;
        internal const int ITEM_ALIQUOTA_IPI_DECIMAIS = 2;

        internal const string ITEM_SIMPLES_NACIONAL = "CST SIMPLES NACIONAL";
        internal const int ITEM_SIMPLES_NACIONAL_INICIO = 128;
        internal const int ITEM_SIMPLES_NACIONAL_TAMANHO = 1;
        internal static int?[] ITEM_SIMPLES_NACIONAL_CSTS = { 300, 400, 500 };

        internal const string ITEM_IDENTIFICACAO2 = "IDENTIFICACAO2";
        internal const int ITEM_IDENTIFICACAO2_INICIO = 129;
        internal const int ITEM_IDENTIFICACAO2_TAMANHO = 30;

        internal const string ITEM_GTIN = "GTIN";
        internal const int ITEM_GTIN_INICIO = 159;
        internal const int ITEM_GTIN_TAMANHO = 20;
        internal static int[] ITEM_GTIN_TAMANHOS = { 8, 12, 13, 14 };

        internal const string ITEM_ENTRADA_CST_PIS = "CST PIS - ENTRADA";
        internal const int ITEM_ENTRADA_CST_PIS_INICIO = 179;
        internal const int ITEM_ENTRADA_CST_PIS_TAMANHO = 3;

        internal const string ITEM_ENTRADA_CST_COFINS = "CST COFINS - ENTRADA";
        internal const int ITEM_ENTRADA_CST_COFINS_INICIO = 182;
        internal const int ITEM_ENTRADA_CST_COFINS_TAMANHO = 3;

        internal const string ITEM_SAIDA_CST_PIS = "CST PIS - SAÍDA";
        internal const int ITEM_SAIDA_CST_PIS_INICIO = 185;
        internal const int ITEM_SAIDA_CST_PIS_TAMANHO = 3;

        internal const string ITEM_SAIDA_CST_COFINS = "CST COFINS - SAÍDA";
        internal const int ITEM_SAIDA_CST_COFINS_INICIO = 188;
        internal const int ITEM_SAIDA_CST_COFINS_TAMANHO = 3;

        internal const string ITEM_CODIGO_PIS_COFINS = "CÓDIGO PIS/COFINS";
        internal const int ITEM_CODIGO_PIS_COFINS_INICIO = 191;
        internal const int ITEM_CODIGO_PIS_COFINS_TAMANHO = 20;
        internal static int?[] ITEM_CODIGOS_PIS_COFINS = { 2, 3, 4, 5, 6, 7, 8, 9 };

        internal const string ITEM_ALIQUOTA_PIS = "ALÍQUOTA PIS";
        internal const int ITEM_ALIQUOTA_PIS_INICIO = 211;
        internal const int ITEM_ALIQUOTA_PIS_TAMANHO = 7;
        internal const int ITEM_ALIQUOTA_PIS_DECIMAIS = 4;
        internal static int?[] ITEM_ALIQUOTA_PIS_CSTS = { 2, 3 };

        internal const string ITEM_ALIQUOTA_COFINS = "ALÍQUOTA COFINS";
        internal const int ITEM_ALIQUOTA_COFINS_INICIO = 218;
        internal const int ITEM_ALIQUOTA_COFINS_TAMANHO = 7;
        internal const int ITEM_ALIQUOTA_COFINS_DECIMAIS = 4;
        internal static int?[] ITEM_ALIQUOTA_COFINS_CSTS = { 2, 3 };

        internal const string ITEM_ANP = "CÓDIGO ANP";
        internal const int ITEM_ANP_INICIO = 225;
        internal const int ITEM_ANP_TAMANHO = 10;

        internal const string ITEM_DESCRICAO2 = "DESCRIÇÃO 2";
        internal const int ITEM_DESCRICAO2_INICIO = 235;
        internal const int ITEM_DESCRICAO2_TAMANHO = 60;

        internal const string ITEM_CEST = "CÓDIGO CEST";
        internal const int ITEM_CEST_INICIO = 295;
        internal const int ITEM_CEST_TAMANHO = 7;

        internal const string ITEM_TIPO_DE_MEDICAMENTO = "TIPO DE MEDICAMENTO";
        internal const int ITEM_TIPO_DE_MEDICAMENTO_INICIO = 302;
        internal const int ITEM_TIPO_DE_MEDICAMENTO_TAMANHO = 1;
        internal static int?[] ITEM_TIPO_DE_MEDICAMENTO_TIPOS = { 0, 1, 2 };

        internal const string ITEM_REFERENCIA_BC = "REFERÊNCIA BC";
        internal const int ITEM_REFERENCIA_BC_INICIO = 303;
        internal const int ITEM_REFERENCIA_BC_TAMANHO = 1;
        internal static int?[] ITEM_REFERENCIAS_BC = { 0, 1, 2, 3, 4 };

        internal const string ITEM_BRANCOS2 = "BRANCOS";
        internal const int ITEM_BRANCOS2_INICIO = 304;
        internal const int ITEM_BRANCOS2_TAMANHO = 189;

        #endregion

        #region FatorDeConversao

        internal const string FATOR_CODIGO = "CÓDIGO";
        internal const int FATOR_CODIGO_INCIO = 0;
        internal const int FATOR_CODIGO_TAMANHO = 10;
        internal const uint FATOR_CODIGO_MAXIMO = 2147483648;

        internal const string FATOR_IDENTIFICACAO = "IDENTIFICAÇÃO";
        internal const int FATOR_IDENTIFICACAO_INICIO = 11;
        internal const int FATOR_IDENTIFICACAO_TAMANHO = 15;

        internal const string FATOR_UNIDADE_CONVERSAO = "UNIDADE DE CONVERSÃO NA COMPRA/VENDA";
        internal const int FATOR_UNIDADE_CONVERSAO_INICIO = 26;
        internal const int FATOR_UNIDADE_CONVERSAO_TAMANHO = 6;

        internal const string FATOR_CONVERSAO = "FATOR DE CONVERSÃO";
        internal const int FATOR_CONVERSAO_INICIO = 32;
        internal const int FATOR_CONVERSAO_TAMANHO = 16;
        internal const int FATOR_CONVERSAO_DECIMAIS = 6;

        internal const string FATOR_BRANCOS = "BRANCOS";
        internal const int FATOR_BRANCOS_INICIO = 48;
        internal const int FATOR_BRANCOS_TAMANHO = 446;

        #endregion
    }
}
