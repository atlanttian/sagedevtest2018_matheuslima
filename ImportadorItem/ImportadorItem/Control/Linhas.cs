﻿using ImportadorItem.DAL;
using ImportadorItem.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ImportadorItem.Control
{
    public class Linhas
    {
        internal static Linha Validar(string linhaCompleta, int nLinha)
        {
            try
            {
                if (linhaCompleta.Length == Consts.QUANTIDADE_DE_CARACTERES)
                {
                    string seq = linhaCompleta.Substring(Consts.SEQUENCIA_INICIO, Consts.SEQUENCIA_TAMANHO);
                    if (SomenteNumeros(seq))
                    {
                        int sequencia = Convert.ToInt32(seq);
                        if (sequencia == nLinha)
                        {
                            int tipo = Convert.ToInt32(linhaCompleta.Substring(Consts.TIPO_DE_REGISTRO_INICIO, Consts.TIPO_DE_REGISTRO_TAMANHO));
                            if (Consts.TIPO_DE_REGISTROS.Contains(tipo))
                            {
                                Linha linha = new Linha(nLinha) { Sequencia = sequencia, Tipo = tipo, LinhaCompleta = linhaCompleta };
                                switch (tipo)
                                {
                                    case 1:
                                        return ValidarUnidadeMedida(linha);
                                    case 2:
                                        return ValidarItem(linha);
                                    case 3:
                                        return ValidarFatorDeConversao(linha);
                                    default:
                                        throw new Exception("Tipo inválido para a validação.");
                                }
                            }
                            else
                                return new Linha(nLinha).ReportarErro($"O campo {Consts.TIPO_DE_REGISTRO} inválido encontrado: {tipo} esperado: {string.Join(" ou ", Consts.TIPO_DE_REGISTROS)}");
                        }
                        else
                            return new Linha(nLinha).ReportarErro($"O campo {Consts.SEQUENCIA} inválido encontrado: {sequencia.ToString("000000")} esperado: {nLinha.ToString("000000")}");
                    }
                    else
                        return new Linha(nLinha).ReportarErro($"O campo {Consts.SEQUENCIA} permite apenas números.");
                }
                else
                    return new Linha(nLinha).ReportarErro($"Quantidade de caracteres inválida encontrado: {linhaCompleta.Length} esperado: {Consts.QUANTIDADE_DE_CARACTERES}");
            }
            catch (Exception ex)
            {
                return new Linha(nLinha).ReportarErro(ex.Message);
            }

        }

        internal static void Importar(Linha linha)
        {
            if (linha is UnidadeMedida unidade)
            {
                DatabaseControl.InserirUnidade(unidade);
            }
            else if (linha is Item item)
            {
                DatabaseControl.InserirItem(item);
            }
            else if (linha is FatorDeConversao fator)
            {
                DatabaseControl.InserirFator(fator);
            }
            else
            {
                throw new Exception("Tipo inválido para ser importado.");
            }
        }

        private static FatorDeConversao ValidarFatorDeConversao(Linha linha)
        {
            FatorDeConversao fator = new FatorDeConversao(linha);

            //CÓDIGO
            string cod = linha.LinhaCompleta.Substring(Consts.FATOR_CODIGO_INCIO, Consts.FATOR_CODIGO_TAMANHO).Replace(" ", "");
            if (!string.IsNullOrWhiteSpace(cod) & cod.Length == Consts.FATOR_CODIGO_TAMANHO)
            {
                if (SomenteNumeros(cod))
                {
                    uint codigo = Convert.ToUInt32(cod);
                    if (codigo < Consts.FATOR_CODIGO_TAMANHO)
                    {
                        if (!DatabaseControl.ExisteFator(codigo))
                            fator.Codigo = codigo;
                        else
                            fator.ReportarErro($"O campo {Consts.FATOR_CODIGO} com o valor de {codigo} já existe na base de dados.");
                    }
                    else
                        fator.ReportarErro($"O campo {Consts.FATOR_CODIGO} com o valor de {codigo} excedeu o valor máximo de {Consts.ITEM_CODIGO_MAXIMO}.");
                }
                else
                    fator.ReportarErro($"O campo {Consts.FATOR_CODIGO} permite apenas números.");
            }
            else
                fator.ReportarErro($"O campo {Consts.FATOR_CODIGO} é de preencimento obrigatório e não pode conter espaços.");

            //IDENTIFICACAO
            fator.Identificaco = linha.LinhaCompleta.Substring(Consts.FATOR_IDENTIFICACAO_INICIO, Consts.FATOR_IDENTIFICACAO_TAMANHO);

            //UNIDADE
            string un = linha.LinhaCompleta.Substring(Consts.FATOR_UNIDADE_CONVERSAO_INICIO, Consts.FATOR_UNIDADE_CONVERSAO_TAMANHO).Trim();
            if (!string.IsNullOrWhiteSpace(un))
            {
                fator.Unidade = un;
            }
            else
                fator.ReportarErro($"O campo {Consts.FATOR_UNIDADE_CONVERSAO} é de preencimento obrigatório.");

            //FATOR
            string ftr = linha.LinhaCompleta.Substring(Consts.FATOR_CONVERSAO_INICIO, Consts.FATOR_CONVERSAO_TAMANHO).Replace(" ", "");
            if (!string.IsNullOrWhiteSpace(ftr) & ftr.Length == Consts.FATOR_CONVERSAO_TAMANHO)
            {
                if (SomenteNumeros(ftr))
                {
                    fator.Fator = Convert.ToSingle(ftr.Insert(ftr.Length - Consts.FATOR_CONVERSAO_DECIMAIS, Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                    if (fator.Fator <= 0)
                        fator.ReportarErro($"O campo {Consts.FATOR_CONVERSAO} deve ser maior que zero.");
                }
                else
                    fator.ReportarErro($"O campo {Consts.FATOR_CONVERSAO} permite apenas números.");
            }
            else
                fator.ReportarErro($"O campo {Consts.FATOR_CONVERSAO} é de preencimento obrigatório e não pode conter espaços.");
            
            return fator;
        }
        
        private static Item ValidarItem(Linha linha)
        {
            Item item = new Item(linha);

            //CÓDIGO
            string cod = linha.LinhaCompleta.Substring(Consts.ITEM_CODIGO_INICIO, Consts.ITEM_CODIGO_TAMANHO).Replace(" ","");//usado replace para remover os espaços dentro do número assim invalidando o campo
            if (!string.IsNullOrWhiteSpace(cod) & cod.Length == Consts.ITEM_CODIGO_TAMANHO)
            {
                if (SomenteNumeros(cod))
                {
                    uint codigo = Convert.ToUInt32(cod);
                    if (codigo < Consts.ITEM_CODIGO_MAXIMO)
                    {
                        if (!DatabaseControl.ExisteItem(codigo))
                            item.Codigo = codigo;
                        else
                            item.ReportarErro($"O campo {Consts.ITEM_CODIGO} com o valor de {codigo} já existe na base de dados.");
                    }
                    else
                        item.ReportarErro($"O campo {Consts.ITEM_CODIGO} com o valor de {codigo} excedeu o valor máximo de {Consts.ITEM_CODIGO_MAXIMO}.");
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_CODIGO} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_CODIGO} é de preencimento obrigatório e não pode conter espaços.");

            //DESCRICAO
            item.Descricao = linha.LinhaCompleta.Substring(Consts.ITEM_DESCRICAO_INICIO, Consts.ITEM_DESCRICAO_TAMANHO).Trim();

            //NCM
            string ncm = linha.LinhaCompleta.Substring(Consts.ITEM_NCM_INICIO, Consts.ITEM_NCM_TAMANHO).Trim();
            if (!string.IsNullOrWhiteSpace(ncm) & ncm.Length == Consts.ITEM_NCM_TAMANHO)
            {
                if (SomenteNumeros(ncm))
                    item.NCM = Convert.ToInt32(ncm);
                else
                    item.ReportarErro($"O campo {Consts.ITEM_NCM} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_NCM} é de preencimento obrigatório e não pode conter espaços.");

            //UNIDADE 1
            item.Unidade = linha.LinhaCompleta.Substring(Consts.ITEM_UNIDADE1_INICIO, Consts.ITEM_UNIDADE1_TAMANHO).Trim();

            //PESO
            string peso = linha.LinhaCompleta.Substring(Consts.ITEM_PESO_INICIO, Consts.ITEM_PESO_TAMANHO).Replace(" ", ""); 
            if (!string.IsNullOrWhiteSpace(peso) & peso.Length == Consts.ITEM_PESO_TAMANHO)
            {
                if (SomenteNumeros(peso))
                {
                    item.Peso = Convert.ToSingle(peso.Insert(peso.Length - Consts.ITEM_PESO_DECIMAIS, Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator)); 
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_PESO} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_PESO} é de preencimento obrigatório e não pode conter espaços.");

            //IDENTIFACAO
            item.Identificacao = linha.LinhaCompleta.Substring(Consts.ITEM_IDENTIFICACAO_INICIO, Consts.ITEM_IDENTIFICACAO_TAMANHO).Trim();

            //TIPO
            string tipo = linha.LinhaCompleta.Substring(Consts.ITEM_TIPO_DO_PRODUTO_INICIO, Consts.ITEM_TIPO_DO_PRODUTO_TAMANHO).Replace(" ", "");
            if (!string.IsNullOrWhiteSpace(tipo) & tipo.Length == Consts.ITEM_TIPO_DO_PRODUTO_TAMANHO)
            {
                if (SomenteNumeros(tipo))
                {
                    int nTipo = Convert.ToInt32(tipo);
                    if (Consts.ITEM_TIPOS_DO_PRODUTO.Contains(nTipo))
                    {
                        item.TipoDoProduto = nTipo;
                    }
                    else
                        item.ReportarErro($"O campo {Consts.ITEM_TIPO_DO_PRODUTO} inválido encontrado: {tipo} esperado: {string.Join(" ou ", Consts.ITEM_TIPOS_DO_PRODUTO.Select(i => string.Format("{0:D2}", i)))}");
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_TIPO_DO_PRODUTO} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_TIPO_DO_PRODUTO} não pode estar em branco.");

            //BRNACOS 1
            string brancos = linha.LinhaCompleta.Substring(Consts.ITEM_BRANCOS1_INICIO, Consts.ITEM_BRANCOS1_TAMANHO).Trim();
            if (!string.IsNullOrWhiteSpace(brancos))
            {
                item.ReportarErro($"Encontrado o valor {brancos} no campo {Consts.ITEM_BRANCOS1} o menso deve estar completamente em branco.");
            }

            //ST ICMS ENTRADA
            string st_icms_entrada = linha.LinhaCompleta.Substring(Consts.ITEM_ENTRADA_ICMS_INICIO, Consts.ITEM_ENTRADA_ICMS_TAMANHO).Replace(" ", "");
            if (st_icms_entrada.Length == Consts.ITEM_ENTRADA_ICMS_TAMANHO)
            {
                if (SomenteNumeros(st_icms_entrada))
                {
                    item.EntradaICMSST = Convert.ToInt32(st_icms_entrada);
                    if (item.EntradaICMSST == 0)
                        item.EntradaICMSST = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ENTRADA_ICMS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ENTRADA_ICMS} não pode conter espaços.");

            //ST IPI ENTRADA
            string st_ipi_entrada = linha.LinhaCompleta.Substring(Consts.ITEM_ENTRADA_IPI_INICIO, Consts.ITEM_ENTRADA_IPI_TAMANHO).Replace(" ", "");
            if (st_ipi_entrada.Length == Consts.ITEM_ENTRADA_IPI_TAMANHO)
            {
                if (SomenteNumeros(st_ipi_entrada))
                {
                    item.EntradaIPIST = Convert.ToInt32(st_ipi_entrada);
                    if (item.EntradaIPIST == 0)
                        item.EntradaIPIST = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ENTRADA_IPI} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ENTRADA_IPI} não pode conter espaços.");

            //ST ICMS SAIDA
            string st_icms_saida = linha.LinhaCompleta.Substring(Consts.ITEM_SAIDA_ICMS_INICIO, Consts.ITEM_SAIDA_ICMS_TAMANHO).Replace(" ", "");
            if (st_icms_saida.Length == Consts.ITEM_SAIDA_ICMS_TAMANHO)
            {
                if (SomenteNumeros(st_icms_saida))
                {
                    item.SaidaICMSST = Convert.ToInt32(st_icms_saida);
                    if (item.SaidaICMSST == 0)
                        item.SaidaICMSST = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_SAIDA_ICMS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_SAIDA_ICMS} não pode conter espaços.");

            //ST IPI SAIDA
            string st_ipi_saida = linha.LinhaCompleta.Substring(Consts.ITEM_SAIDA_IPI_INICIO, Consts.ITEM_SAIDA_IPI_TAMANHO).Replace(" ", "");
            if (st_ipi_saida.Length == Consts.ITEM_SAIDA_IPI_TAMANHO)
            {
                if (SomenteNumeros(st_ipi_saida))
                {
                    item.SaidaIPIST = Convert.ToInt32(st_ipi_saida);
                    if (item.SaidaIPIST == 0)
                        item.SaidaIPIST = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_SAIDA_IPI} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_SAIDA_IPI} não pode conter espaços.");

            //UNIDADE 2
            if (string.IsNullOrWhiteSpace(item.Unidade))
            {
                item.Unidade = linha.LinhaCompleta.Substring(Consts.ITEM_UNIDADE2_INICIO, Consts.ITEM_UNIDADE2_TAMANHO).Trim();
            }

            //ALIQ ICMS
            string aliq_icms = linha.LinhaCompleta.Substring(Consts.ITEM_ALIQUOTA_ICMS_INICIO, Consts.ITEM_ALIQUOTA_ICMS_TAMANHO).Replace(" ", "");
            if (aliq_icms.Length == Consts.ITEM_ALIQUOTA_ICMS_TAMANHO)
            {
                if (SomenteNumeros(aliq_icms))
                {
                    item.AliquotaICMS = Convert.ToSingle(aliq_icms.Insert(aliq_icms.Length - Consts.ITEM_ALIQUOTA_ICMS_DECIMAIS, Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_ICMS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_ICMS} não pode conter espaços.");

            //ALIQ IPI
            string aliq_ipi = linha.LinhaCompleta.Substring(Consts.ITEM_ALIQUOTA_IPI_INICIO, Consts.ITEM_ALIQUOTA_IPI_TAMANHO).Replace(" ", "");
            if (aliq_ipi.Length == Consts.ITEM_ALIQUOTA_IPI_TAMANHO)
            {
                if (SomenteNumeros(aliq_ipi))
                {
                    item.AliquotaIPI = Convert.ToSingle(aliq_ipi.Insert(aliq_ipi.Length - Consts.ITEM_ALIQUOTA_ICMS_DECIMAIS, Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_IPI} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_IPI} não pode conter espaços.");

            //SIMPLES
            string simples = linha.LinhaCompleta.Substring(Consts.ITEM_SIMPLES_NACIONAL_INICIO, Consts.ITEM_SIMPLES_NACIONAL_TAMANHO).Trim();
            if (Consts.ITEM_SIMPLES_NACIONAL_CSTS.Contains(item.EntradaICMSST) || Consts.ITEM_SIMPLES_NACIONAL_CSTS.Contains(item.SaidaICMSST))
            {
                if (!string.IsNullOrWhiteSpace(simples))
                    item.SimplesNacional = simples[0] == 'S';
                else
                    item.ReportarErro($"O campo {Consts.ITEM_SIMPLES_NACIONAL} é de preencimento obrigatório quando os campos {Consts.ITEM_ENTRADA_ICMS} ou {Consts.ITEM_SAIDA_ICMS} possui um dos valores : {string.Join(" ou ", Consts.ITEM_SIMPLES_NACIONAL_CSTS)}");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(simples))
                    item.SimplesNacional = null;
                else
                    item.ReportarErro($"O campo {Consts.ITEM_SIMPLES_NACIONAL} só deve ser preenchido quando os campos {Consts.ITEM_ENTRADA_ICMS} ou {Consts.ITEM_SAIDA_ICMS} possui um dos valores : {string.Join(" ou ", Consts.ITEM_SIMPLES_NACIONAL_CSTS)}");
            }

            item.Identificacao = linha.LinhaCompleta.Substring(Consts.ITEM_IDENTIFICACAO2_INICIO, Consts.ITEM_IDENTIFICACAO2_TAMANHO).Trim();

            //GTIN
            string gtin = linha.LinhaCompleta.Substring(Consts.ITEM_GTIN_INICIO, Consts.ITEM_GTIN_TAMANHO).Replace(" ", "");
            if (!string.IsNullOrWhiteSpace(gtin))
            {
                if (Consts.ITEM_GTIN_TAMANHOS.Contains(gtin.Length))
                {
                    if (SomenteNumeros(gtin))
                    {
                        if (ValidaGTIN(gtin)) 
                        {
                            item.GTIN = gtin;
                        }
                        else
                            item.ReportarErro($"O campo {Consts.ITEM_GTIN} não possiu um código válido.");
                    }
                    else
                        item.ReportarErro($"O campo {Consts.ITEM_GTIN} permite apenas números.");
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_GTIN} não pode conter espaços e deve possuir {string.Join(" ou ", Consts.ITEM_GTIN_TAMANHOS)} dígitos");
            }

            //CST PIS ENTRADA
            string cst_pis_entrada = linha.LinhaCompleta.Substring(Consts.ITEM_ENTRADA_CST_PIS_INICIO, Consts.ITEM_ENTRADA_CST_PIS_TAMANHO).Replace(" ", "");
            if (cst_pis_entrada.Length == Consts.ITEM_ENTRADA_CST_PIS_TAMANHO)
            {
                if (SomenteNumeros(cst_pis_entrada))
                {
                    item.EntradaCSTPIS = Convert.ToInt32(cst_pis_entrada);
                    if (item.EntradaCSTPIS == 0)
                        item.EntradaCSTPIS = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ENTRADA_CST_PIS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ENTRADA_CST_PIS} não pode conter espaços.");

            //CST COFINS ENTRADA
            string cst_cofins_entrada = linha.LinhaCompleta.Substring(Consts.ITEM_ENTRADA_CST_COFINS_INICIO, Consts.ITEM_ENTRADA_CST_COFINS_TAMANHO).Replace(" ", "");
            if (cst_cofins_entrada.Length == Consts.ITEM_ENTRADA_CST_COFINS_TAMANHO)
            {
                if (SomenteNumeros(cst_cofins_entrada))
                {
                    item.EntradaCSTCOFINS = Convert.ToInt32(cst_cofins_entrada);
                    if (item.EntradaCSTCOFINS == 0)
                        item.EntradaCSTCOFINS = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ENTRADA_CST_COFINS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ENTRADA_CST_COFINS} não pode conter espaços.");

            //CST PIS SAIDA
            string cst_pis_saida = linha.LinhaCompleta.Substring(Consts.ITEM_SAIDA_CST_PIS_INICIO, Consts.ITEM_SAIDA_CST_PIS_TAMANHO).Replace(" ", "");
            if (cst_pis_saida.Length == Consts.ITEM_SAIDA_CST_PIS_TAMANHO)
            {
                if (SomenteNumeros(cst_pis_saida))
                {
                    item.SaidaCSTPIS = Convert.ToInt32(cst_pis_saida);
                    if (item.SaidaCSTPIS == 0)
                        item.SaidaCSTPIS = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_SAIDA_CST_PIS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_SAIDA_CST_PIS} não pode conter espaços.");

            //CST COFINS SAIDA
            string cst_cofins_saida = linha.LinhaCompleta.Substring(Consts.ITEM_SAIDA_CST_COFINS_INICIO, Consts.ITEM_SAIDA_CST_COFINS_TAMANHO).Replace(" ", "");
            if (cst_cofins_saida.Length == Consts.ITEM_SAIDA_CST_COFINS_TAMANHO)
            {
                if (SomenteNumeros(cst_cofins_saida))
                {
                    item.SaidaCSTCOFINS = Convert.ToInt32(cst_cofins_saida);
                    if (item.SaidaCSTCOFINS == 0)
                        item.SaidaCSTCOFINS = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_SAIDA_CST_COFINS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_SAIDA_CST_COFINS} não pode conter espaços.");

            //CÓDIGO PIS/COFINS
            string codigo_pis_cofins = linha.LinhaCompleta.Substring(Consts.ITEM_CODIGO_PIS_COFINS_INICIO, Consts.ITEM_CODIGO_PIS_COFINS_TAMANHO).Trim();
            if (Consts.ITEM_CODIGOS_PIS_COFINS.Contains(item.SaidaCSTPIS) || Consts.ITEM_CODIGOS_PIS_COFINS.Contains(item.SaidaCSTCOFINS))
            {
                if (!string.IsNullOrWhiteSpace(codigo_pis_cofins))
                {
                    item.CodigoPISCOFINS = codigo_pis_cofins;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_CODIGO_PIS_COFINS} não pode estar vazio quando os campos {Consts.ITEM_SAIDA_CST_PIS} e/ou {Consts.ITEM_SAIDA_CST_COFINS} possuirm um dos valores : {string.Join(" ou ", Consts.ITEM_CODIGOS_PIS_COFINS.Select(i => string.Format("{0:D2}", i)))}");
            }
            else
            {
                if (string.IsNullOrWhiteSpace(codigo_pis_cofins))
                    item.CodigoPISCOFINS = null;
                else
                    item.CodigoPISCOFINS = codigo_pis_cofins;
            }

            //ALIQ PIS
            string aliq_pis = linha.LinhaCompleta.Substring(Consts.ITEM_ALIQUOTA_PIS_INICIO, Consts.ITEM_ALIQUOTA_PIS_TAMANHO).Replace(" ", "");
            if (aliq_pis.Length == Consts.ITEM_ALIQUOTA_PIS_TAMANHO)
            {
                if (SomenteNumeros(aliq_pis))
                {
                    item.AliquotaPIS = Convert.ToSingle(aliq_pis.Insert(aliq_pis.Length - Consts.ITEM_ALIQUOTA_PIS_DECIMAIS, Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                    if (item.AliquotaPIS == 0)
                    {
                        if (!Consts.ITEM_ALIQUOTA_PIS_CSTS.Contains(item.SaidaCSTPIS))
                            item.AliquotaPIS = null;
                        else
                            item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_PIS} não pode estar zerado quando o campo {Consts.ITEM_SAIDA_CST_PIS} possuir o valor {string.Join(" ou ", Consts.ITEM_ALIQUOTA_PIS_CSTS.Select(i => string.Format("{0:D3}", i)))}");
                    }
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_PIS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_PIS} não pode conter espaços.");

            //ALIQ COFINS
            string aliq_cofins = linha.LinhaCompleta.Substring(Consts.ITEM_ALIQUOTA_COFINS_INICIO, Consts.ITEM_ALIQUOTA_COFINS_TAMANHO).Replace(" ", "");
            if (aliq_cofins.Length == Consts.ITEM_ALIQUOTA_COFINS_TAMANHO)
            {
                if (SomenteNumeros(aliq_cofins))
                {
                    item.AliquotaCOFINS = Convert.ToSingle(aliq_cofins.Insert(aliq_cofins.Length - Consts.ITEM_ALIQUOTA_COFINS_DECIMAIS, Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                    if (item.AliquotaCOFINS == 0)
                    {
                        if (!Consts.ITEM_ALIQUOTA_COFINS_CSTS.Contains(item.SaidaCSTCOFINS))
                            item.AliquotaCOFINS = null;
                        else
                            item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_COFINS} não pode estar zerado quando o campo {Consts.ITEM_SAIDA_CST_COFINS} possuir o valor {string.Join(" ou ", Consts.ITEM_ALIQUOTA_COFINS_CSTS.Select(i => string.Format("{0:D3}", i)))}");
                    }
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_COFINS} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ALIQUOTA_COFINS} não pode conter espaços.");

            //ANP
            string anp = linha.LinhaCompleta.Substring(Consts.ITEM_ANP_INICIO, Consts.ITEM_ANP_TAMANHO).Replace(" ", "");
            if (anp.Length == Consts.ITEM_ANP_TAMANHO)
            {
                if (SomenteNumeros(anp))
                {
                    item.ANP = Convert.ToUInt32(anp);
                    if (item.ANP == 0)
                        item.ANP = null;
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_ANP} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_ANP} não pode conter espaços.");

            //DEACRICAO 2
            if (string.IsNullOrWhiteSpace(item.Descricao))
            {
                item.Descricao = linha.LinhaCompleta.Substring(Consts.ITEM_DESCRICAO2_INICIO, Consts.ITEM_DESCRICAO2_TAMANHO).Trim();
            }

            //CEST
            string cest = linha.LinhaCompleta.Substring(Consts.ITEM_CEST_INICIO, Consts.ITEM_CEST_TAMANHO).Replace(" ", "");
            if (cest.Length == Consts.ITEM_CEST_TAMANHO)
            {
                if (SomenteNumeros(cest))
                {
                    item.CEST = Convert.ToInt32(cest);
                    if (item.CEST == 0)
                    {
                        item.CEST = null;
                    }
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_CEST} permite apenas números.");
            }
            else
                item.ReportarErro($"O campo {Consts.ITEM_CEST} não pode conter espaços.");

            //TIPO DE MEDICAMENTO
            string tipoM = linha.LinhaCompleta.Substring(Consts.ITEM_TIPO_DE_MEDICAMENTO_INICIO, Consts.ITEM_TIPO_DE_MEDICAMENTO_TAMANHO).Replace(" ", "");
            if (!string.IsNullOrWhiteSpace(tipoM))
            {
                if (SomenteNumeros(tipoM))
                {
                    int t = Convert.ToInt32(tipoM);
                    if (Consts.ITEM_TIPO_DE_MEDICAMENTO_TIPOS.Contains(t))
                    {
                        item.TipoMedicamento = t;
                    }
                    else
                        item.ReportarErro($"O campo {Consts.ITEM_TIPO_DE_MEDICAMENTO} inválido encontrado: {t} esperado: {string.Join(" ou ", Consts.ITEM_TIPO_DE_MEDICAMENTO_TIPOS)}");
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_TIPO_DE_MEDICAMENTO} permite apenas números.");
            }
            else
                item.TipoMedicamento = null;

            //REFERÊNCIA BC
            string refBC = linha.LinhaCompleta.Substring(Consts.ITEM_REFERENCIA_BC_INICIO, Consts.ITEM_REFERENCIA_BC_TAMANHO).Replace(" ", "");
            if (!string.IsNullOrWhiteSpace(refBC))
            {
                if (SomenteNumeros(refBC))
                {
                    int t = Convert.ToInt32(refBC);
                    if (Consts.ITEM_REFERENCIAS_BC.Contains(t))
                    {
                        item.ReferenciaBC = t;
                    }
                    else
                        item.ReportarErro($"O campo {Consts.ITEM_REFERENCIA_BC} inválido encontrado: {t} esperado: {string.Join(" ou ", Consts.ITEM_REFERENCIAS_BC)}");
                }
                else
                    item.ReportarErro($"O campo {Consts.ITEM_REFERENCIA_BC} permite apenas números.");
            }
            else
                item.ReferenciaBC = null;

            return item;
        }

        private static UnidadeMedida ValidarUnidadeMedida(Linha linha)
        {
            UnidadeMedida unidade = new UnidadeMedida(linha);

            string un = linha.LinhaCompleta.Substring(Consts.UNIDADE_INICIO, Consts.UNIDADE_TAMANHO).Trim();
            if (!string.IsNullOrWhiteSpace(un))
            {
                if (!DatabaseControl.ExisteUnidade(un))
                    unidade.Unidade = un;
                else
                    unidade.ReportarErro($"O campo {Consts.UNIDADE} com o valor de {un} já existe na base de dados.");
            }
            else
                unidade.ReportarErro($"O campo {Consts.UNIDADE} não pode estar em branco.");

            string descricao = linha.LinhaCompleta.Substring(Consts.UNIDADE_DESCRICAO_INICIO, Consts.UNIDADE_TAMANHO).Trim();
            if (!descricao.Equals(un, StringComparison.OrdinalIgnoreCase))
            {
                unidade.Descricao = descricao;

                string brancos = linha.LinhaCompleta.Substring(Consts.UNIDADE_BRANCOS_INICIO, Consts.UNIDADE_BRANCOS_TAMANHO).Trim();
                if (!string.IsNullOrWhiteSpace(brancos))
                    unidade.ReportarErro($"Encontrado o valor {brancos} no campo {Consts.UNIDADE_BRANCOS} o menso deve estar completamente em branco.");
            }
            else
                unidade.ReportarErro($"O campo {Consts.UNIDADE_DESCRICAO} não pode ser igual ao campo {Consts.UNIDADE}.");

            return unidade;
        }

        private static bool SomenteNumeros(string valor)
        {
            return !new Regex("[^0-9]+").IsMatch(valor);
        }

        private static bool ValidaGTIN(string gtin)
        {
            switch (gtin.Length)
            {
                case 8:
                    gtin = "000000" + gtin;
                    break;
                case 12:
                    gtin = "00" + gtin;
                    break;
                case 13:
                    gtin = "0" + gtin;
                    break;
                case 14:
                    break;
                default:
                    return false;
            }

            //Calculando dígito verificador
            int[] a = new int[13];
            a[0] = int.Parse(gtin[0].ToString()) * 3;
            a[1] = int.Parse(gtin[1].ToString());
            a[2] = int.Parse(gtin[2].ToString()) * 3;
            a[3] = int.Parse(gtin[3].ToString());
            a[4] = int.Parse(gtin[4].ToString()) * 3;
            a[5] = int.Parse(gtin[5].ToString());
            a[6] = int.Parse(gtin[6].ToString()) * 3;
            a[7] = int.Parse(gtin[7].ToString());
            a[8] = int.Parse(gtin[8].ToString()) * 3;
            a[9] = int.Parse(gtin[9].ToString());
            a[10] = int.Parse(gtin[10].ToString()) * 3;
            a[11] = int.Parse(gtin[11].ToString());
            a[12] = int.Parse(gtin[12].ToString()) * 3;
            int sum = a[0] + a[1] + a[2] + a[3] + a[4] + a[5] + a[6] + a[7] + a[8] + a[9] + a[10] + a[11] + a[12];
            int check = (10 - (sum % 10)) % 10;

            //Checando
            int last = int.Parse(gtin[13].ToString());
            return check == last;
        }

    }
}
