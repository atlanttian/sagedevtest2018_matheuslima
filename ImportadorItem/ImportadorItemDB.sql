USE [master]
GO
/****** Object:  Database [ImportadorItens]    Script Date: 7/23/2018 10:08:00 PM ******/
CREATE DATABASE [ImportadorItens] ON  PRIMARY 
( NAME = N'ImportadorItens', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017DEV\MSSQL\DATA\ImportadorItens.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ImportadorItens_log', FILENAME = N'D:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017DEV\MSSQL\DATA\ImportadorItens_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ImportadorItens].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ImportadorItens] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ImportadorItens] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ImportadorItens] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ImportadorItens] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ImportadorItens] SET ARITHABORT OFF 
GO
ALTER DATABASE [ImportadorItens] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ImportadorItens] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ImportadorItens] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ImportadorItens] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ImportadorItens] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ImportadorItens] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ImportadorItens] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ImportadorItens] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ImportadorItens] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ImportadorItens] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ImportadorItens] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ImportadorItens] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ImportadorItens] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ImportadorItens] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ImportadorItens] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ImportadorItens] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ImportadorItens] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ImportadorItens] SET RECOVERY FULL 
GO
ALTER DATABASE [ImportadorItens] SET  MULTI_USER 
GO
ALTER DATABASE [ImportadorItens] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ImportadorItens] SET DB_CHAINING OFF 
GO
EXEC sys.sp_db_vardecimal_storage_format N'ImportadorItens', N'ON'
GO
USE [ImportadorItens]
GO
/****** Object:  Table [dbo].[FatorDeConversao]    Script Date: 7/23/2018 10:08:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FatorDeConversao](
	[Codigo] [int] NOT NULL,
	[Identificacao] [varchar](15) NULL,
	[Unidade] [varchar](6) NOT NULL,
	[Fator] [float] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 7/23/2018 10:08:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[Codigo] [int] NOT NULL,
	[Descricao] [varchar](60) NULL,
	[NCM] [int] NOT NULL,
	[Unidade] [varchar](6) NULL,
	[Peso] [float] NOT NULL,
	[Identificacao] [varchar](30) NULL,
	[TipoDoProduto] [int] NULL,
	[EntradaICMSST] [int] NULL,
	[EntradaIPIST] [int] NULL,
	[SaidaICMSST] [int] NULL,
	[SaidaIPIST] [int] NULL,
	[AliquotaICMS] [float] NULL,
	[AliquotaIPI] [float] NULL,
	[SimplesNacional] [char](1) NULL,
	[GTIN] [numeric](20, 0) NULL,
	[EntradaCSTPIS] [int] NULL,
	[EntradaCSTCOFINS] [int] NULL,
	[SaidaCSTPIS] [int] NULL,
	[SaidaCSTCOFINS] [int] NULL,
	[CodigoPISCOFINS] [varchar](20) NULL,
	[AliquotaPIS] [float] NULL,
	[AliquotaCOFINS] [float] NULL,
	[ANP] [nchar](10) NULL,
	[CEST] [int] NULL,
	[TipoMedicamento] [int] NULL,
	[ReferenciaBC] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Codigo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Unidade]    Script Date: 7/23/2018 10:08:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unidade](
	[Unidade] [varchar](6) NOT NULL,
	[Descricao] [varchar](30) NOT NULL,
 CONSTRAINT [PK_Unidade] PRIMARY KEY CLUSTERED 
(
	[Unidade] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [Unidade]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [TipoDoProduto]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [EntradaICMSST]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [EntradaIPIST]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [SaidaICMSST]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [SaidaIPIST]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [SimplesNacional]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [EntradaCSTPIS]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [EntradaCSTCOFINS]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [SaidaCSTPIS]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [SaidaCSTCOFINS]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [AliquotaPIS]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [AliquotaCOFINS]
GO
ALTER TABLE [dbo].[Item] ADD  DEFAULT (NULL) FOR [CEST]
GO
USE [master]
GO
ALTER DATABASE [ImportadorItens] SET  READ_WRITE 
GO
